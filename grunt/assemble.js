module.exports = function (grunt, data) {
    return {
        options:{
            layoutdir: data.assembleLayoutDir,
            flatten: true,
            layout: 'default.hbs',
            partials: data.assemblePartials
        },
        page: {
            'files': data.assemblePageFiles
        }
    };
};
