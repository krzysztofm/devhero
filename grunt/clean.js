module.exports = function (grunt, data) {
    return {
        css: [data.cleanCSS],
        pages: [data.cleanPages]
    };
};