module.exports = function (grunt, data) {
    return {
        img: {
            options: {
                optimizationLevel: 3,
                cache: false
            },
            files: [{
                expand: true,
                cwd: 'source/images',
                src: ['**/*.{png,jpg,gif}'],
                dest: 'images/'
            }]
        },
        imgContent: {
            options: {
                optimizationLevel: 3,
                cache: false
            },
            files: [{
                expand: true,
                cwd: 'source/images-content',
                src: ['**/*.{png,jpg,gif}'],
                dest: 'images-content/'
            }]
        }
    };
};