module.exports = function (grunt, data) {
    return {
        sass: {
            options: {
                sourceMap: false
            },
            files: [{
                expand: true,
                cwd: data.sassCwd,
                src: '**/*.sass',
                dest: data.sassDest,
                ext: '.css'
            }]
        }
    };
};