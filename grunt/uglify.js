module.exports = function (grunt, data) {
    return {
        various: {
            options: {
                compress: false,
                mangle: false,
                beautify: true,
                preserveComments: 'all'
            },
            files: data.uglify.various
        },
        vendors : {
            options: {
                compress: true,
                mangle: true,
                beautify: false,
                preserveComments: 'some'
            },
            files: data.uglify.files
        }

    };
};