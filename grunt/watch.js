module.exports = function (grunt, data) {
    return {
        js: {
            files: [ data.jsSource + '/**/*.js'],
            tasks: ['uglify']
        },
        sass: {
            files: [data.sassCwd + '/**/*.sass'],
            tasks: ['clean:css', 'sass', 'notify:build']
        },
        assemble: {
            files: ['source/templates/**/*.hbs'],
            tasks: ['clean:pages', 'assemble', 'notify:build']
        },
        images: {
            files: ['source/images/**/*.{png,jpg,gif}', 'source/images-content/**/*.{png,jpg,gif}'],
            tasks: ['imagemin', 'notify:build']
        }

    };
};