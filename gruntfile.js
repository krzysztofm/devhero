module.exports = function(grunt) {

    var options = {
        assembleLayoutDir : 'source/templates/layouts',
        assemblePartials : 'source/templates/partials/**/*.hbs',
        assemblePageFiles : {'pages/': ['source/templates/pages/*.hbs']},
        cleanCSS: ['css/**/*.css'],
        cleanPages: ['pages/**/*.html'],
        sassCwd : 'source/sass',
        sassDest : 'css',
        jsSource : 'source/js',
        uglify : {
            various: {'js/various.js': ['source/js/various.js']},
            files: {
                'js/various.lib.min.js':   [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/enquire/dist/enquire.js',
                    'bower_components/gsap/src/uncompressed/TweenMax.js',

                    'bower_components/ScrollMagic/scrollmagic/uncompressed/ScrollMagic.js',
                    'bower_components/ScrollMagic/scrollmagic/uncompressed/plugins/animation.gsap.js',
                    'bower_components/bx-slider.js/dist/jquery.bxslider.js'
                ]
            }
        }
    };

    grunt.loadNpmTasks('assemble');

    // load grunt config
    require('load-grunt-config')(grunt, {
        data: options
    });

    // simplified example
    grunt.registerTask('default', ['clean', 'notify:build']);
    grunt.registerTask('build', ['clean', 'sass', 'assemble', 'uglify', 'imagemin', 'notify:build']);


};