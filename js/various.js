var APP;

APP = {
    init: function() {
        enquire.register("screen and (max-width:991px)", {
            match: function() {
                APP.animateRotate(".animate-rotate-single", ".big-icon");
                APP.lazyload($(".lazyload"));
            }
        }).register("screen and (min-width:992px)", {
            match: function() {
                APP.animateRotate(".animate-rotate", ".big-icon");
                APP.lazyload($(".lazyload"));
                APP.lazyload($(".lazyload-desktop"));
            }
        });
        this.stickyHeader();
        this.toggleMenuRwd();
        this.animateContact();
        this.portfolioSlider();
    },
    lazyload: function(el) {
        var img = el, imgLength = img.length;
        function loadImg(img) {
            var $el = $(img), imgUrl = $el.data("img-url");
            if (imgUrl === "undefined") {
                return false;
            }
            if ($el.is("img")) {
                $el.attr("src", imgUrl);
            } else {
                $el.css({
                    "background-image": "url(" + imgUrl + ")"
                });
            }
        }
        for (var i = 0; i < imgLength; i++) {
            loadImg(img[i]);
        }
    },
    stickyHeader: function() {
        window.addEventListener("scroll", function(e) {
            var distanceY = window.pageYOffset || document.documentElement.scrollTop, shrinkOn = 300, header = $("#sticky-header");
            if (distanceY > shrinkOn) {
                header.addClass("sticky-header--fixed");
            } else {
                header.removeClass("sticky-header--fixed");
            }
        });
    },
    toggleMenuRwd: function() {
        document.getElementById("main-header-toggle").addEventListener("click", function() {
            document.getElementById("main-header-menu-mobile").classList.toggle("open");
            this.classList.toggle("open");
        });
    },
    animateRotate: function(elsContainer, elAnimate) {
        var controller = new ScrollMagic.Controller({
            globalSceneOptions: {
                reverse: false
            }
        }), elements = document.querySelectorAll(elsContainer);
        for (var i = 0; i < elements.length; i++) {
            (function(i) {
                var animation = new TweenMax.staggerFrom(elements[i].querySelectorAll(elAnimate), .5, {
                    scaleX: 0,
                    scaleY: 0,
                    rotation: 360
                }, .2);
                var scene = new ScrollMagic.Scene({
                    triggerElement: elements[i]
                }).setTween(animation).addTo(controller);
            })(i);
        }
    },
    animateContact: function() {
        var controller, timeline, scene;
        if ($(".js-animation-note").length === 0) {
            return false;
        }
        controller = new ScrollMagic.Controller({
            globalSceneOptions: {
                reverse: false,
                //triggerHook: 'onEnter',
                offset: -50
            }
        });
        timeline = new TimelineMax();
        timeline.append(TweenMax.fromTo(".note", .5, {
            xPercent: -50
        }, {
            xPercent: 0
        }));
        timeline.to(".contact-form__phone", .1, {
            x: -7,
            ease: Quad.easeInOut
        });
        timeline.to(".contact-form__phone", .1, {
            repeat: 4,
            x: 7,
            yoyo: true,
            delay: .1,
            ease: Quad.easeInOut
        });
        scene = new ScrollMagic.Scene({
            triggerElement: ".js-animation-note"
        }).setTween(timeline).addTo(controller);
    },
    portfolioSlider: function() {
        var elBgHolder = $(".wide-bg-bxslider"), elSlider = $("#portfolio-slider"), elFirstSlider = elSlider.find("li").first();
        function setColor(el) {
            var colors = el.data("bg");
            if (elBgHolder.length > 0) {
                elBgHolder.css(bgGradiend(colors));
            }
        }
        function bgGradiend(colors) {
            var color1 = colors[0], color2 = colors[1];
            return {
                background: color1,
                background: "-moz-linear-gradient(-45deg, " + color1 + " 0%, " + color2 + " 100%)",
                background: "-webkit-gradient(linear, left top, right bottom, color-stop(0%," + color1 + "), color-stop(100%," + color2 + "))",
                background: "-webkit-linear-gradient(-45deg, " + color1 + " 0%," + color2 + " 100%)",
                background: "-o-linear-gradient(-45deg, " + color1 + "  0%," + color2 + " 100%)",
                background: "-ms-linear-gradient(-45deg, " + color1 + " 0%," + color2 + " 100%)",
                background: "linear-gradient(135deg, " + color1 + "  0%," + color2 + " 100%)"
            };
        }
        setColor(elFirstSlider);
        elSlider.bxSlider({
            pager: false,
            minSlides: 1,
            maxSlides: 1,
            nextSelector: "#image-slider-ce-next",
            prevSelector: "#image-slider-ce-prev",
            onSliderLoad: function() {},
            onSlideBefore: function() {
                var elCurrentIndex = this.getCurrentSlide();
                elBgHolder.css({
                    opacity: .8
                });
                setColor(this.getSlideElement(elCurrentIndex));
            },
            onSlideAfter: function() {
                elBgHolder.css({
                    opacity: 1
                });
            }
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    APP.init();
}, false);